import Vue from 'vue'
import Router from 'vue-router'
import PageLayout from '@/components/PageLayout'

Vue.use(Router)

export default new Router({
  mode: 'history',
  // baseUrl: process.env.NODE_ENV === 'production' ? '/project' : '/',
  base: '/project',
  routes: [
    {
      path: '/',
      name: 'indexPage',
      component: PageLayout
    },
    {
      path: '*',
      name: 'otherPage',
      component: PageLayout
    }
  ]
})
